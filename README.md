# guardian-system

java版看门狗守护系统，可自由添加被守护的进程对象，使用用户自定义的脚本监视和重启对象，高度自由，理论上可监视不同启动方式和不同运行环境的进程对象，如：普通进程对象、docker容器对象等；
另外，该系统具有邮箱发送通知功能，用户可自定义邮箱信息，当被看护进程挂掉，会以邮箱的方式通知到用户；

### 技术交流群：1134935268

![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/150101_7cc6e393_1754888.png "屏幕截图.png")

## 扫码进群

| QQ群  | ![输入图片说明](https://images.gitee.com/uploads/images/2020/0806/170702_9a893d57_1754888.png "屏幕截图.png") | 微信群  | ![输入图片说明](https://images.gitee.com/uploads/images/2020/0817/092412_df43384d_1754888.png "屏幕截图.png")) |
|---|---|---|---|

#### 使用说明

1. 利用maven将程序打包
2. 使用src/resources/gs 文件启动 或java -jar的方式启动程序
3. 使用sh gs -h 或则 sh gs -help 查看命令使用

提示：： 源码中有一个《示例目录》，其中包含有可执行到jar ，和linux平台下示例脚本

#### 提示：
gs 为linux下的一个启动脚本，如有在windows平台使用的，可使用如下命令：
  exp1. 启动程序
    java -jar guardian-system.jar &
  exp2. 帮助文档
    java -jar guardian-system.jar -help 或则 java -jar guardian-system.jar -h 

#### 示例：
以linux下使用为例：
1. 添加任务

提示：：： 执行文件最好使用绝对路径，而执行命令最好封装到一个可执行文件中，避免因路径或执行命令太复杂而造成的失败；
提示：：： 执行文件注意赋予可执行权限
```
./gs -add test.jar -probe sh /home/probe -start /home/start

## 检查test.jar是否存活的示例probe文件内容
## 复制一下内容，把test.jar修改为监控任务运行时的进程名字即可；
#!/bin/sh  
para1=`ps -ef | grep test.jar| grep -v grep`  
if [ ! "$para1" ]; then  
  echo "dead"  
else  
  echo "test.jar"  
fi 

### 启动命令文件start 的示例内容
### 若是其它应用，将该名称修改为对应的启动命令即可
java -jar /home/test.jar &

```

2. 查看任务

```
./gs -list
## 结果展示
./gs -list
        application name: a.jar
        probe command: echo 1 
        start command: echo 2 
        restart success count: 2
        restart fail count: 0
```

3. 删除命令

```
./gs -remove test.jar
```

4. 添加通知邮箱

```
## -user 这里添加的是开通了邮件通知服务的邮箱的账户和密码；
## -to 这里添加的是接收通知的用户的邮箱，可多个，使用空格分隔 

sh gs -mail -user test@163.com -pwd test -to 1@163.com 2@qq.com  n@qq.com
```

5. 查看配置内容


```
./gs -config
```

6. 启动时监听端口修改
默认为9766端口
```
./gs -p 9766 
```





































