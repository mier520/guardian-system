package com.guardian.communication;

/**
 * 系统通讯接口
 * @author: huwei
 * @date: 2019/10/11 9:09
 * @version: 1.0.0
 */
public interface CommunicationService {

    /**
     * 异步的启动通讯服务
     * @param port 监听端口
     */
    default void asyncStartup(int port){
        new Thread(()->{
            syncStartup(port);
        }).start();
    }

    /**
     * 同步的启动通讯服务
     * @param port 监听端口
     */
    void syncStartup(int port);
    
}
