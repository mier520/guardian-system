package com.guardian;


import com.guardian.command.CommandManager;
import com.guardian.communication.nettry.NettyCommunicationService;
import com.guardian.exception.InitializationException;
import com.guardian.monitor.GuardianObjectMonitor;

import java.util.Arrays;
import java.util.Map;


/**
 * 启动类
 *
 * @author: huwei
 * @date: 2019/10/10 11:20
 * @version: 1.0.0
 */
public class GuardianSystemApplication {



    public static void main(String[] args) throws InitializationException {
        //初始化执行命令
        CommandManager.initExecutorCommand();

        //初始化启动参数
        StartupArgument.initStartupArgument(args);

        Map<String ,String[]> startupCommandMap = StartupArgument.argumentMap();
        execStartupCommand(startupCommandMap);

        //启动监听器
        new GuardianObjectMonitor().asyncMonitor();

        System.out.println("communication starting...");
        //启动通讯中心
        new NettyCommunicationService().asyncStartup(StartupArgument.port());
        System.out.println("communication started success ,and port ".concat(String.valueOf(StartupArgument.port())));


        System.out.println("GuardianSystemApplication start up...");
    }

    private static void execStartupCommand(Map<String, String[]> startupCommandMap) {
        startupCommandMap.forEach((command ,arg)->{
            CommandManager.startupCommand(command ,arg);
        });
    }
}
