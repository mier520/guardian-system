package com.guardian.exception;

/**
 *
 * @author: huwei
 * @date: 2019/10/10 13:17
 * @version: 1.0.0
 */
public class InitializationException extends Exception {

    public InitializationException(String msg){
        this(msg ,null);
    }

    public InitializationException(String msg ,Exception e){
        super(msg ,e);
    }
}
