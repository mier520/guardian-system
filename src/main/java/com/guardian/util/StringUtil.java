package com.guardian.util;

/**
 * 字符串工具
 * @author: huwei
 * @date: 2019/10/10 13:02
 * @version: 1.0.0
 */
public class StringUtil {

    /**
     * 将字符串数组使用指定连接符链接成字符串表现形式
     * @param str 字符串数组
     * @param joinMark 链接符号
     * @return 字符串表现形式
     */
    public static String arrayToString(String[] str ,String joinMark){
        StringBuffer stringBuffer = new StringBuffer();
        for(String s : str){
            stringBuffer.append(s).append(joinMark);
        }
        return stringBuffer.toString();
    }
}
