package com.guardian.command.startup;

import com.guardian.command.Command;
import com.guardian.command.CommandManager;

/**
 * 启动帮助命令
 * @author: huwei
 * @date: 2019/10/11 10:09
 * @version: 1.0.0
 */
public class HelpStartupCommand implements Command {

    @Override
    public String[] command() {
        return new String[]{"-help","-h"};
    }

    @Override
    public String intro() {
        return "Shows a list of all available commands";
    }

    @Override
    public String format() {
        return null;
    }

    @Override
    public String usage() {
        return "./gs -help -h ";
    }

    @Override
    public String exec(String... args) {
        System.out.println(CommandManager.executorCommand(CommandManager.PRINT_HELP_WORD));
        System.exit(0);
        return null;
    }
}
