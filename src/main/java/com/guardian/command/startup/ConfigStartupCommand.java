package com.guardian.command.startup;

import com.guardian.command.Command;
import com.guardian.command.util.CommandUtil;

/**
 * 配置参数查看命令
 * @author: huwei
 * @date: 2019/10/10 14:54
 * @version: 1.0.0
 */
public class ConfigStartupCommand implements Command {

    @Override
    public String[] command() {
        return new String[]{"-config"};
    }

    @Override
    public String intro() {
        return "Displays a list of configured parameters";
    }

    @Override
    public String format() {
        return "./gs -config";
    }

    @Override
    public String usage() {
        return "./gs -config";
    }

    @Override
    public String exec(String... args) {
        System.out.println(CommandUtil.sendMessageToMain("config" ,null));
        System.exit(0);
        return null;
    }
}
