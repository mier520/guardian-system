package com.guardian.command.startup;

import com.guardian.command.Command;
import com.guardian.command.util.CommandUtil;

/**
 * 移除守护应用命令
 * @author: huwei
 * @date: 2019/10/10 14:54
 * @version: 1.0.0
 */
public class RemoveGuardianObjectStartupCommand implements Command {

    @Override
    public String[] command() {
        return new String[]{"-remove"};
    }

    @Override
    public String intro() {
        return "Remove application from monitor";
    }

    @Override
    public String format() {
        return "./gs -remove [application name]";
    }

    @Override
    public String usage() {
        return "./gs -remove test.jar ";
    }

    @Override
    public String exec(String... args) {
        if(args == null || args.length == 0){
            return "command fail,exp\r\n".concat(commandIntro());
        }
        System.out.println(CommandUtil.sendMessageToMain("remove" ,args));
        System.exit(0);
        return null;
    }
}
