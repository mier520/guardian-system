package com.guardian.command.startup;

import com.guardian.command.Command;

/**
 * 端口命令
 * @author: huwei
 * @date: 2019/10/11 10:09
 * @version: 1.0.0
 */
public class PortStartupCommand implements Command {

    @Override
    public String[] command() {
        return new String[]{"-p" ,"-P"};
    }

    @Override
    public String intro() {
        return "The system listens on the port setting, which is 9766 by default";
    }

    @Override
    public String format() {
        return "./gs -p [port] or -P [port]";
    }

    @Override
    public String usage() {
        return "./gs -p 9766 or -P 9766";
    }

    @Override
    public String exec(String... args) {
        return args[0];
    }
}
