package com.guardian.command.startup;

import com.guardian.StartupArgument;
import com.guardian.command.Command;
import com.guardian.command.util.CommandUtil;
import com.guardian.communication.CommunicationClient;
import com.guardian.util.StringUtil;

import java.io.IOException;

/**
 * 添加守护对象命令
 * @author: huwei
 * @date: 2019/10/10 14:54
 * @version: 1.0.0
 */
public class AddGuardianObjectStartupCommand implements Command {

    private final String addCommandSymbol = "-add";
    private final String probeCommandSymbol = "-probe";
    private final String startCommandSymbol = "-start";

    @Override
    public String[] command() {
        return new String[]{"-add"};
    }

    @Override
    public String intro() {
        return "add guardian object info \r\n -probe The script that probes the application alive returns the application name that you set";
    }

    @Override
    public String format() {
        return "./gs -add [application name] -probe [Verify the application's surviving scripts] -start [Script to launch the application]";
    }

    @Override
    public String usage() {
        return "./gs -add test.jar -probe echo test.jar -start java -jar test.jar";
    }

    @Override
    public String exec(String... args) {
        if(args == null || args.length != 1 || !StartupArgument.containsArgument(probeCommandSymbol)
            || !StartupArgument.containsArgument(startCommandSymbol)){
            exit();
            return null;
        }
        String[] probeArg = StartupArgument.getArgument(probeCommandSymbol);
        String[] startArg = StartupArgument.getArgument(startCommandSymbol);
        String[] addArgs = new String[3];
        addArgs[0] = args[0];
        addArgs[1] = StringUtil.arrayToString(probeArg ," ");
        addArgs[2] = StringUtil.arrayToString(startArg ," ");

        System.out.println(CommandUtil.sendMessageToMain("add" ,addArgs));
        System.exit(0);
        return null;
    }
}
