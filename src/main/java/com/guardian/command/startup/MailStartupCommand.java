package com.guardian.command.startup;

import com.guardian.StartupArgument;
import com.guardian.command.Command;
import com.guardian.command.util.CommandUtil;
import com.guardian.email.MailUtil;
import com.guardian.util.StringUtil;

/**
 * 邮件配置启动命令
 * @author: huwei
 * @date: 2019/10/10 14:54
 * @version: 1.0.0
 */
public class MailStartupCommand implements Command {

    @Override
    public String[] command() {
        return new String[]{"-mail"};
    }

    @Override
    public String intro() {
        return "Message configuration parameters for notification messages \r\n -to Multiple receivers are separated by character Spaces";
    }

    @Override
    public String format() {
        return "./gs -mail -host [mail server default stmp.163.com] -port [mail port default 465] -user [mail login user] -pwd [mail login password] -to [mail receiver] -from [mail sender default -user value] -protocol [mail protocol default stmp] ";
    }

    @Override
    public String usage() {
        return "./gs -mail -user 1@163.com -pwd 1123456 -to 2@163.com";
    }

    @Override
    public String exec(String... args) {
        String[] user = StartupArgument.getArgument("-user");
        String[] password = StartupArgument.getArgument("-pwd");
        String[] to = StartupArgument.getArgument("-to");
        if(user == null || user.length == 0 || password == null || password.length == 0 || to == null || to.length == 0){
           exit();
        }
        String[] host = StartupArgument.getArgument("-host");
        String[] port = StartupArgument.getArgument("-port");
        String[] protocol = StartupArgument.getArgument("-protocol");
        String[] from = StartupArgument.getArgument("-from");

        String[] mailArgs = new String[7];
        mailArgs[0] = host == null || host.length == 0 || host[0] == null ? MailUtil.MAIL_SERVER_SMTP_163 : host[0];
        mailArgs[1] = port == null || port.length == 0 || port[0] == null ? String.valueOf(MailUtil.MAIL_SERVER_PORT) : port[0];
        mailArgs[2] = user[0];
        mailArgs[3] = password[0];
        mailArgs[4] = StringUtil.arrayToString(to ," ");
        mailArgs[5] = from == null || from.length == 0 || from[0] == null ? user[0] : from[0];
        mailArgs[6] = protocol == null || protocol.length == 0 || protocol[0] == null ? MailUtil.MAIL_SERVER_PROTOCOL : protocol[0];

        System.out.println(CommandUtil.sendMessageToMain("mail" ,mailArgs));
        System.exit(0);
        return null;
    }
}
