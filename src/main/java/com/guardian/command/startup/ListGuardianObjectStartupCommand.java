package com.guardian.command.startup;

import com.guardian.StartupArgument;
import com.guardian.command.Command;
import com.guardian.command.util.CommandUtil;
import com.guardian.communication.CommunicationClient;
import com.guardian.util.StringUtil;

import java.io.IOException;

/**
 * 输出守护应用列表
 * @author: huwei
 * @date: 2019/10/10 14:54
 * @version: 1.0.0
 */
public class ListGuardianObjectStartupCommand implements Command {

    @Override
    public String[] command() {
        return new String[]{"-list"};
    }

    @Override
    public String intro() {
        return "Displays a list of all current daemon applications";
    }

    @Override
    public String format() {
        return null;
    }

    @Override
    public String usage() {
        return "./gs -list ";
    }

    @Override
    public String exec(String... args) {
        System.out.println(CommandUtil.sendMessageToMain("list" ,null));
        System.exit(0);
        return null;
    }
}
