package com.guardian.command.constant;

/**
 * @author: huwei
 * @date: 2019/10/11 16:27
 * @version: 1.0.0
 */
public class CommandConstant {

    public static final String EXECUTOR_COMMAND_SPILT_SYMBOL = "\t";
}
