package com.guardian.command;

/**
 * 命令执行接口
 * @author: huwei
 * @date: 2019/10/10 13:28
 * @version: 1.0.0
 */
public interface Command {
    /**
     * 换行符号
     */
    static final String LINE_BREAK = "\r\n";


    /**
     * 命令名称
     * @return 命令名称
     */
    String[] command();

    /**
     * 简要的介绍命令
     * @return 命令介绍信息
     */
    String intro();

    /**
     * 命令的使用格式
     * @return
     */
    String format();

    /**
     * 命令使用介绍
     * @return 使用描述
     */
    String usage();

    /**
     * 执行命令操作
     * @param args 命令参数列表
     * @return 返回命令执行提示
     */
    String exec(String ...args);

    /**
     * 退出系统
     */
    default void exit(){
        System.out.println("command fail,exp \r\n".concat(commandIntro()));
        System.exit(0);
    }

    /**
     * 展示命令的使用描述
     * @return
     */
    default String commandIntro( ){
        StringBuffer sb = new StringBuffer();

        //命令名称
        sb.append(LINE_BREAK).append("command: ");
        String[] commandNames = command();
        for (String commandName : commandNames) {
            sb.append(commandName).append(" ");
        }
        sb.append(LINE_BREAK);

        //命令简介
        sb.append("intro: ").append(LINE_BREAK);
        sb.append("\t").append(intro()).append(LINE_BREAK);

        //命令的使用格式
        if(format() != null) {
            sb.append("format: ").append(LINE_BREAK);
            sb.append("\t").append(format()).append(LINE_BREAK);
        }

        //命令使用描述
        sb.append("usage: ").append(LINE_BREAK);
        sb.append("\t").append(usage()).append(LINE_BREAK);

        return sb.toString();
    }
}
