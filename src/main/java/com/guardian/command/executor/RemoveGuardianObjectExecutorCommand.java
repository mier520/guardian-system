package com.guardian.command.executor;

import com.guardian.buffer.GuardianObjectBuffer;
import com.guardian.command.startup.RemoveGuardianObjectStartupCommand;


/**
 * 移除应用命令
 * @author: huwei
 * @date: 2019/10/10 14:54
 * @version: 1.0.0
 */
public class RemoveGuardianObjectExecutorCommand extends RemoveGuardianObjectStartupCommand {

    @Override
    public String[] command() {
        return new String[]{"remove"};
    }

    @Override
    public String exec(String... args) {
        if(args == null || args.length <= 0){
            exit();
        }
        GuardianObjectBuffer.remove(args[0]);
        return args[0].concat(" remove ok");
    }
}
