package com.guardian.command.executor;

import com.guardian.buffer.MonitorBuffer;
import com.guardian.command.startup.MailStartupCommand;

/**
 * mail 执行命令
 * @author: huwei
 * @date: 2019/10/10 14:54
 * @version: 1.0.0
 */
public class MailExecutorCommand extends MailStartupCommand {

    @Override
    public String[] command() {
        return new String[]{"mail"};
    }

    @Override
    public String exec(String... args) {
        if(args == null || args.length != 7){
           exit();
        }
        String host = args[0];
        String port = args[1];
        String user = args[2];
        String password = args[3];
        String to = args[4];
        String from = args[5];
        String protocol = args[6];

        MonitorBuffer.add(MonitorBuffer.MonitorBufferName.MAIL_SERVER_HOST ,host);
        MonitorBuffer.add(MonitorBuffer.MonitorBufferName.MAIL_SERVER_PORT ,port);
        MonitorBuffer.add(MonitorBuffer.MonitorBufferName.MAIL_SERVER_USERNAME ,user);
        MonitorBuffer.add(MonitorBuffer.MonitorBufferName.MAIL_SERVER_PASSWORD ,password);
        MonitorBuffer.add(MonitorBuffer.MonitorBufferName.MAIL_TO ,to);
        MonitorBuffer.add(MonitorBuffer.MonitorBufferName.MAIL_FROM ,from);
        MonitorBuffer.add(MonitorBuffer.MonitorBufferName.MAIL_SERVER_PROTOCOL ,protocol);

        return "set mail config ok";
    }
}
