package com.guardian.command.executor;

import com.guardian.buffer.GuardianObjectBuffer;
import com.guardian.command.startup.AddGuardianObjectStartupCommand;
import com.guardian.entity.GuardianObject;

/**
 * 添加守护对象命令
 * @author: huwei
 * @date: 2019/10/10 14:54
 * @version: 1.0.0
 */
public class AddGuardianObjectExecutorCommand extends AddGuardianObjectStartupCommand {

    @Override
    public String[] command() {
        return new String[]{"add"};
    }

    @Override
    public String exec(String... args) {
        if(args == null || args.length != 3){
            exit();
        }
        GuardianObject guardianObject = new GuardianObject(args[0] ,args[1] ,args[2]);
        GuardianObjectBuffer.add(guardianObject);
        return "add ok";
    }
}
