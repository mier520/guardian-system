package com.guardian.command.executor;

import com.guardian.buffer.MonitorBuffer;
import com.guardian.command.startup.ConfigStartupCommand;

/**
 * 查看配置文件命令
 * @author: huwei
 * @date: 2019/10/10 14:54
 * @version: 1.0.0
 */
public class ConfigExecutorCommand extends ConfigStartupCommand {

    @Override
    public String[] command() {
        return new String[]{"config"};
    }

    @Override
    public String exec(String... args) {
        String configList = MonitorBuffer.showConfig();
        return configList.length() == 0 ? "none" : configList;
    }
}
