package com.guardian.command.executor;

import com.guardian.buffer.GuardianObjectBuffer;
import com.guardian.command.startup.ListGuardianObjectStartupCommand;


/**
 * 输出守护应用列表
 * @author: huwei
 * @date: 2019/10/10 14:54
 * @version: 1.0.0
 */
public class ListGuardianObjectExecutorCommand extends ListGuardianObjectStartupCommand {

    @Override
    public String[] command() {
        return new String[]{"list"};
    }

    @Override
    public String exec(String... args) {
        String appList = GuardianObjectBuffer.listGuardianObject();
        return appList.length() == 0 ? "none" : appList;
    }
}
