package com.guardian.constant;

import java.io.File;

/**
 * 系统固定变量
 * @author: huwei
 * @date: 2019/10/10 12:58
 * @version: 1.0.0
 */
public class SystemConstant {

    /**
     * 系统的运行根目录
     */
    public static String SYSTEM_HOME_DIRECTORY;

    static{
        SYSTEM_HOME_DIRECTORY = new File("").getAbsolutePath();
    }
}
