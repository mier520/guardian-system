package com.guardian.entity;

import lombok.Data;

/**
 * 守护对象数据结构
 * @author: huwei
 * @date: 2019/10/11 13:38
 * @version: 1.0.0
 */
@Data
public class GuardianObject {
    /**
     * 守护对象的名称
     */
    private String name;
    /**
     * 探查守护对象存活的命令，该命令执行后需返回守护对象的名称，否则视为守护对象已死亡，将重启守护对象
     */
    private String detectionCommand;
    /**
     * 启动守护对象的命令
     */
    private String startCommand;
    /**
     * 启动成功次数
     */
    private int startSuccessCount = 0;
    /**
     * 启动失败次数
     */
    private int startFailCount = 0;

    public GuardianObject(String name, String detectionCommand, String startCommand) {
        this.name = name;
        this.detectionCommand = detectionCommand;
        this.startCommand = startCommand;
    }
}
