package com.guardian.buffer;

import com.guardian.command.Command;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 监视对象数据池
 * @author: huwei
 * @date: 2019/10/11 14:13
 * @version: 1.0.0
 */
public class MonitorBuffer {
    /**
     * monitor配置参数名称set
     */
    private static final Set<String> bufferNameSet = new HashSet<>(16);
    /**
     * monitor配置参数映射
     * key为bufferNameSet所存在配置名称
     */
    private static final Map<String ,Object> bufferNameMap = new ConcurrentHashMap<>(16);

    static{
        bufferNameSet.add(MonitorBufferName.IDLE_TIME);
        bufferNameSet.add(MonitorBufferName.CHARSET);
        bufferNameSet.add(MonitorBufferName.MAIL_SERVER_HOST);
        bufferNameSet.add(MonitorBufferName.MAIL_SERVER_PORT);
        bufferNameSet.add(MonitorBufferName.MAIL_SERVER_PROTOCOL);
        bufferNameSet.add(MonitorBufferName.MAIL_SERVER_USERNAME);
        bufferNameSet.add(MonitorBufferName.MAIL_SERVER_PASSWORD);
        bufferNameSet.add(MonitorBufferName.MAIL_FROM);
        bufferNameSet.add(MonitorBufferName.MAIL_TO);
    }

    /**
     * 添加监视器配置参数
     * @param bufferName 配置参数名，该名称必须在
     * @param value
     * @return
     */
    public static final boolean add(String bufferName ,Object value){
        if(bufferNameSet.contains(bufferName)){
            return bufferNameMap.put(bufferName ,value) != null;
        }
        return false;
    }

    public static final Object get(String bufferName ){
        return bufferNameMap.get(bufferName);
    }

    public static final Object get(String bufferName ,Object defaultValue){
        return get(bufferName) == null ? defaultValue : get(bufferName);
    }

    public static final String showConfig(){
        StringBuffer sb = new StringBuffer();
        bufferNameMap.forEach((k,v)->{
            sb.append(k).append("=").append(v).append(Command.LINE_BREAK);
        });
        return sb.toString();
    }

    /**
     * 返回监视器的配置参数名集合
     * @return
     */
    public static final Set<String> bufferName(){
        return Collections.unmodifiableSet(bufferNameSet);
    }

    public static class MonitorBufferName {
        public static final String IDLE_TIME = "idle_time";

        public static final String CHARSET = "charset";

        /**
         * 邮箱服务地址
         */
        public static final String MAIL_SERVER_HOST = "MAIL_SERVER_HOST";
        /**
         * 邮箱服务端口
         */
        public static final String MAIL_SERVER_PORT = "MAIL_SERVER_PORT";
        /**
         * 邮箱服务协议
         */
        public static final String MAIL_SERVER_PROTOCOL = "MAIL_SERVER_PROTOCOL";
        /**
         * 邮箱登录用户
         */
        public static final String MAIL_SERVER_USERNAME = "MAIL_SERVER_USERNAME";
        /**
         * 邮箱登录密码
         */
        public static final String MAIL_SERVER_PASSWORD = "MAIL_SERVER_PASSWORD";
        /**
         * 邮件发送用户
         */
        public static final String MAIL_FROM = "MAIL_FROM";
        /**
         * 邮件接收用户 ，多个用户使用字符空格分割
         */
        public static final String MAIL_TO = "MAIL_TO";
    }
}
